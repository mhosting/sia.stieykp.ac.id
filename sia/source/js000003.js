var AllocationService=function() {
AllocationService.initializeBase(this);
this._timeout = 0;
this._userContext = null;
this._succeeded = null;
this._failed = null;
}
AllocationService.prototype={
_get_path:function() {
 var p = this.get_path();
 if (p) return p;
 else return AllocationService._staticInstance.get_path();},
HelloWorld:function(succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'HelloWorld',false,{},succeededCallback,failedCallback,userContext); },
GetAvailableRooms:function(dayId,periodId,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetAvailableRooms',false,{dayId:dayId,periodId:periodId},succeededCallback,failedCallback,userContext); },
GetAvailableTeachers:function(dayId,periodId,courseId,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetAvailableTeachers',false,{dayId:dayId,periodId:periodId,courseId:courseId},succeededCallback,failedCallback,userContext); },
SaveAllocation:function(dayID,periodID,roomID,courseID,teacherID,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'SaveAllocation',false,{dayID:dayID,periodID:periodID,roomID:roomID,courseID:courseID,teacherID:teacherID},succeededCallback,failedCallback,userContext); },
DeleteAllocation:function(dayID,periodID,roomID,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'DeleteAllocation',false,{dayID:dayID,periodID:periodID,roomID:roomID},succeededCallback,failedCallback,userContext); },
GetCoursesOfTeacher:function(teacherID,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetCoursesOfTeacher',false,{teacherID:teacherID},succeededCallback,failedCallback,userContext); },
GetCoursesOfGrade:function(gradeID,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetCoursesOfGrade',false,{gradeID:gradeID},succeededCallback,failedCallback,userContext); }}
AllocationService.registerClass('AllocationService',Sys.Net.WebServiceProxy);
AllocationService._staticInstance = new AllocationService();
AllocationService.set_path = function(value) { AllocationService._staticInstance.set_path(value); }
AllocationService.get_path = function() { return AllocationService._staticInstance.get_path(); }
AllocationService.set_timeout = function(value) { AllocationService._staticInstance.set_timeout(value); }
AllocationService.get_timeout = function() { return AllocationService._staticInstance.get_timeout(); }
AllocationService.set_defaultUserContext = function(value) { AllocationService._staticInstance.set_defaultUserContext(value); }
AllocationService.get_defaultUserContext = function() { return AllocationService._staticInstance.get_defaultUserContext(); }
AllocationService.set_defaultSucceededCallback = function(value) { AllocationService._staticInstance.set_defaultSucceededCallback(value); }
AllocationService.get_defaultSucceededCallback = function() { return AllocationService._staticInstance.get_defaultSucceededCallback(); }
AllocationService.set_defaultFailedCallback = function(value) { AllocationService._staticInstance.set_defaultFailedCallback(value); }
AllocationService.get_defaultFailedCallback = function() { return AllocationService._staticInstance.get_defaultFailedCallback(); }
AllocationService.set_enableJsonp = function(value) { AllocationService._staticInstance.set_enableJsonp(value); }
AllocationService.get_enableJsonp = function() { return AllocationService._staticInstance.get_enableJsonp(); }
AllocationService.set_jsonpCallbackParameter = function(value) { AllocationService._staticInstance.set_jsonpCallbackParameter(value); }
AllocationService.get_jsonpCallbackParameter = function() { return AllocationService._staticInstance.get_jsonpCallbackParameter(); }
AllocationService.set_path("/esm/AllocationService.asmx");
AllocationService.HelloWorld= function(onSuccess,onFailed,userContext) {AllocationService._staticInstance.HelloWorld(onSuccess,onFailed,userContext); }
AllocationService.GetAvailableRooms= function(dayId,periodId,onSuccess,onFailed,userContext) {AllocationService._staticInstance.GetAvailableRooms(dayId,periodId,onSuccess,onFailed,userContext); }
AllocationService.GetAvailableTeachers= function(dayId,periodId,courseId,onSuccess,onFailed,userContext) {AllocationService._staticInstance.GetAvailableTeachers(dayId,periodId,courseId,onSuccess,onFailed,userContext); }
AllocationService.SaveAllocation= function(dayID,periodID,roomID,courseID,teacherID,onSuccess,onFailed,userContext) {AllocationService._staticInstance.SaveAllocation(dayID,periodID,roomID,courseID,teacherID,onSuccess,onFailed,userContext); }
AllocationService.DeleteAllocation= function(dayID,periodID,roomID,onSuccess,onFailed,userContext) {AllocationService._staticInstance.DeleteAllocation(dayID,periodID,roomID,onSuccess,onFailed,userContext); }
AllocationService.GetCoursesOfTeacher= function(teacherID,onSuccess,onFailed,userContext) {AllocationService._staticInstance.GetCoursesOfTeacher(teacherID,onSuccess,onFailed,userContext); }
AllocationService.GetCoursesOfGrade= function(gradeID,onSuccess,onFailed,userContext) {AllocationService._staticInstance.GetCoursesOfGrade(gradeID,onSuccess,onFailed,userContext); }
