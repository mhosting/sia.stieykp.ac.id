<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=data_mahasiswa.xls");
include_once("reference/config.php");
$qall = "SELECT * FROM msmhs m,kelasparalel_mhs k where k.nimhs=m.NIMHSMSMHS and (m.kdkonsen like '%$konsen%' and m.AGAMA like '%$agama%' and m.TAHUNMSMHS like '%$masuk%' and m.ASSMAMSMHS like '%$propinsi%' and k.nmkelas  like '%$kelas%') order by m.NIMHSMSMHS ASC";
$hasilall = mysql_query($qall);
$jum = mysql_num_rows($hasilall);
?>
<table width="100%">
    <tbody><tr>
            <td>
                <span id="ctl00_ContentPlaceHolder2_Lblexdet"></span>
                <div>
                    <table class="table" rules="all" onsortcommand="SortData" datakeyfield="ID" id="ctl00_ContentPlaceHolder2_dgAsgnm" style="border-color: Silver; border-width: 0px; border-style: solid; width: 100%; border-collapse: collapse;" align="Left" border="1" cellpadding="2" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col" rowspan="2">NO</th>
                                <th scope="col" colspan="14">DATA AKADEMIK MAHASISWA</th>
                                <th scope="col" colspan="7">DATA PRIBADI MAHASISWA</th>
                                <th scope="col" colspan="4">DATA PENDIDIKAN</th>
                                <th scope="col" colspan="4">DATA SEKOLAH</th>
                                <th scope="col" colspan="4">DATA ORTU/WALI</th>
                            </tr>
                            <tr>                                
                                <th scope="col">PROGRAM STUDI</th>
                                <th scope="col">NIM</th>
                                <th scope="col">NAMA</th>
                                <th scope="col">KELAS</th>
                                <th scope="col">KELAS PARAREL</th>
                                <th scope="col">TEMPAT TANGGAL LAHIR</th>
                                <th scope="col">JENIS KELAMIN</th>
                                <th scope="col">STATUS MHS</th>
                                <th scope="col">TAHUN MASUK</th>
                                <th scope="col">BATAS STUDI</th>
                                <th scope="col">TANGGAL MASUK</th>
                                <th scope="col">TANGGAL KELUAR</th>
                                <th scope="col">PROPINSI</th>
                                <th scope="col">STATUS</th>
                                <th scope="col">AGAMA</th>
                                <th scope="col">ALAMAT ASAL</th>
                                <th scope="col">ALAMAT DI YOGYA</th>
                                <th scope="col">TELP/HP</th>
                                <th scope="col">EMAIL</th>
                                <th scope="col">TB</th>
                                <th scope="col">BB</th>
                                <th scope="col">KELAS</th>
                                <th scope="col">JURUSAN</th>
                                <th scope="col">PENDIDIKAN TERAKHIR</th>
                                <th scope="col">TAHUN KELULUSAN</th>
                                <th scope="col">NAMA SEKOLAH</th>
                                <th scope="col">ALAMAT SEKOLAH</th>
                                <th scope="col">PROPINSI</th>
                                <th scope="col">TAHUN KELULUSAN</th>
                                <th scope="col">NAMA ORTU/WALI</th>
                                <th scope="col">PEKERJAAN ORTU/WALI</th>
                                <th scope="col">ALAMAT ORTU/WALI</th>
                                <th scope="col">TELP ORTU/WALI</th>
                            </tr>
                        </thead><tbody>
                            <?
                            $noxcc=0;
                            while($dataall = mysql_fetch_array($hasilall))
                            {
                            $noxcc++;
                            $nim= $dataall["NIMHSMSMHS"];
                            $nama= $dataall["NMMHSMSMHS"];
                            $kelas= $dataall["nmkelas"];
                            $stat= $dataall["STMHSMSMHS"];
                            $stm= $dataall["STPIDMSMHS"];
                            $stmmhs= $dataall["STMHSMSMHS"];
                            $kls= $dataall["SHIFTMSMHS"];
                            $tptlahir= $dataall["TPLHRMSMHS"];
                            $tgllahir = date('d-m-Y', strtotime($dataall["TGLHRMSMHS"]));
                            $jenkel= $dataall["KDJEKMSMHS"];
                            $thmsk= $dataall["TAHUNMSMHS"];
                            $kdpro= $dataall["ASSMAMSMHS"];
                            $btsstudi= $dataall["BTSTUMSMHS"];
                            $tglmsk= date('d-m-Y', strtotime($dataall["TGMSKMSMHS"]));
                            $tgllulus= date('d-m-Y', strtotime($dataall["TGLLSMSMHS"]));
                            $dagama= $dataall["AGAMA"];
                            $dalmlengkap= $dataall["ALAMATLENGKAP"];
                            $dalmyogya= $dataall["ALAMATYOGYA"];
                            $dtelp= $dataall["TELP"];
                            $demail= $dataall["EMAIL"];
                            $dtb= $dataall["TINGGIBADAN"];
                            $dbb= $dataall["BERATBADAN"];
                            $skelas= $dataall["KELAS"];
                            $sjurusan= $dataall["JURUSAN"];
                            $spedidikan= $dataall["PENDIDIKAN"];
                            $stahunlulus= $dataall["TAHUNLULUS"];
                            $pnamasekolah= $dataall["NAMASEKOLAH"];
                            $palmsekolah= $dataall["ALAMATSEKOLAH"];
                            $ppropinsi= $dataall["PROPINSI"];
                            $ptelpsek= $dataall["TELPSEKOLAH"];
                            $onamaortu= $dataall["NAMAORTUWALI"];
                            $opekortu= $dataall["PEKERJAAN ORTU/WALI"];
                            $oalmortu= $dataall["ALAMATORTUWALI"];
                            $otelportu= $dataall["TELPORTUWALI"];
                            $totmk= "SELECT * from statusmhs where tahun='$tahunajaran' and nim='$nim'";
                            $hasilmk = mysql_query($totmk);
                            $datamk = mysql_fetch_array($hasilmk);
                            $jumjum = mysql_num_rows($hasilmk);
                            $stat= $datamk["status"];
                            $tglaktifasi= $datamk["tglaktifasi"];
                            $tglkrs= $datamk["tglkrs"];
                            $tglacc= $datamk["tglacc"];
                            $tglrekap= $datamk["tglrekap"];

                            if($jumjum>=1)
                            {
                            if($stat=="A")
                            {
                            $stats="Aktif";
                            }elseif($stat=="N")
                            {
                            $stats="Non Aktif";
                            }elseif($stat=="D")
                            {
                            $stats="Drop Out";
                            }
                            elseif($stat=="K")
                            {
                            $stats="Keluar";
                            }elseif($stat=="C")
                            {
                            $stats="Cuti";
                            }else
                            {
                            $stats="--";
                            }
                            }else
                            {
                            $stats="--";
                            }
                            ?>
                            <tr>
                                <td align="center" ><? print("$noxcc");?>.</td>
                                <?
                                $pieces = explode("/", $kelas);
                                ?>
                                <td align="center" >
                                    <? if (($pieces[1]) == SMPH){?>
                                    S1 - MANAJEMEN
                                    <? } elseif (($pieces[1]) == SMOF){?>
                                    S1 - MANAJEMEN
                                    <? } elseif (($pieces[1]) == SMRS){?>
                                    S1 - MANAJEMEN
                                    <? } elseif (($pieces[1]) == SMTU){?>
                                    S1 - MANAJEMEN
                                    <? } elseif (($pieces[1]) == SAPK){?>
                                    S1 - AKUNTANSI
                                    <? } elseif (($pieces[1]) == SABK){?>
                                    S1 - AKUNTANSI
                                    <? } elseif (($pieces[1]) == DAPK){?>
                                    D3 - AKUNTANSI
                                    <? } elseif (($pieces[1]) == DABK){?>
                                    D3 - AKUNTANSI
                                    <? } ?>
                                </td>
                                <td align="center" valign="center"><? print("$nim");?></td>
                                <td align="left" valign="center">&nbsp;&nbsp;<? print("$nama");?></td>
                                <td align="center" valign="center"><? print("$kls");?></td>
                                <td>&nbsp;&nbsp;<? print("$kelas");?></td>
                                <td align="center" valign="center"><? print("$tptlahir");?>, <? print("$tgllahir");?></td>
                                <td align="center" valign="center">
                                    <?
                                    if($jenkel=="L")
                                    {
                                    ?>
                                    L - LAKI-LAKI
                                    <?
                                    }else
                                    {
                                    ?>
                                    P - PEREMPUAN
                                    <?
                                    }
                                    ?>
                                <td align="center" valign="center">
                                    <? if($stmmhs=="A"){ ?>
                                    A - AKTIF
                                    <?}elseif($stmmhs=="C"){?>
                                    C - CUTI
                                    <?}elseif($stmmhs=="D"){?>
                                    D - DROP OUT / PUTUS STUDI
                                    <?}elseif($stmmhs=="K"){?>
                                    K - KELUAR
                                    <?}elseif($stmmhs=="L"){?>
                                    L - LULUS
                                    <?}elseif($stmmhs=="N"){?>
                                    N - NON AKTIF
                                    <?}?>
                                </td>
                                <td align="center" valign="center"><? print("$thmsk");?></td>
                                <td align="center" valign="center"><? print("$btsstudi");?></td>
                                <td align="center" valign="center"><? print("$tglmsk");?></td>
                                <td align="center" valign="center"><? print("$tgllulus");?></td>
                                <?
                                $qpeg2 = "SELECT * FROM tbpro where KDPROTBPRO='".$kdpro."'";
                                $datapeg2 = mysql_query($qpeg2);
                                $dataall2 = mysql_fetch_array($datapeg2);
                                $NMPROTBPRO = $dataall2["NMPROTBPRO"];
                                ?>
                                <td align="center" valign="center"><? print("$NMPROTBPRO");?></td>
                                <td align="center" >
                                    <? if($stm=="B"){ ?>
                                    B - BARU
                                    <?}else{?>
                                    P - PINDAHAN
                                    <?}?></td>
                                <td align="center" valign="center">
                                    <? if($dagama=="B"){ ?>
                                    BUDHA
                                    <?}elseif($dagama=="H"){?>
                                    HINDU
                                    <?}elseif($dagama=="I"){?>
                                    ISLAM
                                    <?}elseif($dagama=="K"){?>
                                    KATOLIK
                                    <?}elseif($dagama=="L"){?>
                                    LAIN - LAIN
                                    <?}elseif($dagama=="P"){?>
                                    PROTESTAN
                                    <?}elseif($dagama=="C"){?>
                                    KEPERCAYAAN
                                    <?}?></td>
                                <td align="center" valign="center"><? print("$dalmlengkap");?></td>
                                <td align="center" valign="center"><? print("$dalmyogya");?></td>
                                <td align="center" valign="center"><? print("$dtelp");?></td>
                                <td align="center" valign="center"><? print("$demail");?></td>
                                <td align="center" valign="center"><? print("$dtb");?></td>
                                <td align="center" valign="center"><? print("$dbb");?></td>
                                <td align="center" valign="center"><? print("$skelas");?></td>
                                <td align="center" valign="center"><? print("$sjurusan");?></td>
                                <td align="center" valign="center"><? print("$spedidikan");?></td>
                                <td align="center" valign="center"><? print("$stahunlulus");?></td>
                                <td align="center" valign="center"><? print("$pnamasekolah");?></td>
                                <td align="center" valign="center"><? print("$palmsekolah");?></td>
                                <td align="center" valign="center">
                                    <?
                                    $qpeg2 = "SELECT * FROM tbpro where KDPROTBPRO='".$ppropinsi."'";
                                    $datapeg2 = mysql_query($qpeg2);

                                    $dataall2 = mysql_fetch_array($datapeg2);

                                    $KDPROTBPRO = $dataall2["KDPROTBPRO"];
                                    $NMPROTBPRO = $dataall2["NMPROTBPRO"];
                                    ?>
                                    <? print("$NMPROTBPRO");?></td>
                                <td align="center" valign="center"><? print("$ptelpsek");?></td>
                                <td align="center" valign="center"><? print("$onamaortu");?></td>
                                <td align="center" valign="center"><? print("$opekortu");?></td>
                                <td align="center" valign="center"><? print("$oalmortu");?></td>
                                <td align="center" valign="center"><? print("$otelportu");?></td>
                            </tr>
                            <?

                            }
                            $stat="";
                            $stats="";
                            ?>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>

    </tbody></table>